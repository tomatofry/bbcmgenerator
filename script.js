angular.module('bbcm', []).controller('MainController', function($scope) {
	 $scope.code = [

	 ];

	 $scope.import_code = "";

	 
	 $scope.form_name = the_form_name;
	 $scope.is_form = $scope.form_name.length?true:false;

	 $scope.modal_name = "";

	 $scope.module = {
	 	"name":"",
	 	"class_name":"",
	 	"description":"",
	 	"category":"Theme Modules",
	 	"editor_export":true,
	 	"enabled":true,
	 };


	$scope.addField = function(section){
		var newfield = {
			"slug":"",			
			"type":"text",
			"label":"",
			"default":"",
			"maxlength":"",
			"rows":"",
			"size":"",
			"placeholder":"",
			"class":"",
			"description":"",
			"help":"",
			"show_reset":"",
			"media_buttons":"",
			"show_remove":"",
			"options":[],
			"preview":{
				"type": "text",
				"selector": "",
				"property":"",
				"unit":""
			},
			"form":""
			

		};
		section.fields.push(newfield);
	};

	$scope.getFieldsList = function(){
		var fields = [];
		for(var i in $scope.code){
			var tab = $scope.code[i];
			for(var j in tab.sections){
				var section = tab.sections[j];
				
				for(var k in section.fields){
					var field = section.fields[k];
					fields.push(field);

				}
			}
		}
		return fields;
	}

	$scope.getSectionsList = function(){
		var sections = [];
		for(var i in $scope.code){
			var tab = $scope.code[i];
			for(var j in tab.sections){
				var section = tab.sections[j];
				sections.push(section);
			}
		}
		return sections;
	}

	$scope.getTabsList = function(){
		var tabs = [];
		for(var i in $scope.code){
			var tab = $scope.code[i];
			tabs.push(tab);
		}
		return tabs;
	}


	$scope.updateSlug = function(tab,slug_name,title_name){
		if(!tab[slug_name]){
			tab[slug_name] = tab[title_name];
			tab[slug_name] = tab[slug_name].replace(/ /g,"_");
			tab[slug_name] = tab[slug_name].toLowerCase();
		}
	}

	$scope.$watch("import_code",function(){
		if($scope.import_code){
			var imp;
			try{
				imp = JSON.parse($scope.import_code);
			}
			catch(e){

			}
			if(imp){
				$scope.code = imp.code;
				
				if(imp.module){
					$scope.module = imp.module;
					$scope.is_form = false;
				}
				else{
					$scope.form_name = imp.form_name;
					$scope.modal_name = imp.modal_name;
					$scope.is_form = true;
				}


				$scope.import_code = "";
				$scope.show_import = false;
			}

		}
	});

	$scope.updateArray = function(id, variable){
		var e = document.getElementById(id);
		console.log(id);
		try{
			var code = JSON.parse(e.value);
			// console.log(code);
		}
		catch(e){
			
		}
		
		
		if(!code){
			return;
		}
		for(var i in variable){
			// console.log(code[i]);
			variable[i] = code[i];

		}
		
		e.value = " ";

	};

	$scope.$watch("module.name",function(){
		document.title = $scope.module.name;
	});

});