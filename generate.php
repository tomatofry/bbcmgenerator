<?php
$code = json_decode( $_REQUEST['code'] );
if(isset($_REQUEST['module'])){
	$module = json_decode( $_REQUEST['module'] );	
}
else{
	$form_name = ( $_REQUEST['form_name'] );	
	$modal_name = ( $_REQUEST['modal_name'] );	
}

header("Content-Type: text/plain");
?>
<?php if(isset($_REQUEST['module'])): ?>
<?php echo "<?php"; ?>

class <?php echo $module->class_name; ?> extends FLBuilderModule {


    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('<?php echo $module->name; ?>', 'fl-builder'),
            'description'   => __('<?php echo $module->description; ?>', 'fl-builder'),
            'category'		=> __('<?php echo $module->category; ?>', 'fl-builder'),
            'editor_export' => <?php echo $module->editor_export; ?>, // Defaults to true and can be omitted.
            'enabled'       => <?php echo $module->enabled; ?>, // Defaults to true and can be omitted.
        ));
    }
}
<?php endif; ?>

$options = array(
<?php foreach($code as $tab): ?>
	"<?php echo $tab->slug; ?>" => array(
		"title" => "<?php echo $tab->title; ?>", 
		"sections" => array(
<?php		 foreach($tab->sections as $section): ?>
			"<?php echo $section->slug; ?>" => array(
				"title" => "<?php echo $section->title; ?>",
				"fields" => array(
<?php 				foreach($section->fields as $field): ?>
					"<?php echo $field->slug; ?>" => array(
						"type" => "<?php echo $field->type; ?>",						
						"label" => "<?php echo $field->label; ?>",
						"default" => "<?php echo $field->default; ?>",
						"maxlength" => "<?php echo $field->maxlength; ?>",
						"rows" => "<?php echo $field->rows; ?>",
						"size" => "<?php echo $field->size; ?>",
						"placeholder" => "<?php echo $field->placeholder; ?>",
						"help" => <?php echo $field->help?'"'.$field->help.'"':"NULL"; ?>,
						"class" => "<?php echo $field->class; ?>",
						"description" => "<?php echo $field->description; ?>",
						"show_reset" => "<?php echo $field->show_reset; ?>",
						"media_buttons" => "<?php echo $field->media_buttons; ?>",
						"show_remove" => "<?php echo $field->show_remove; ?>",
						"form" => "<?php if(isset($field->form)) echo $field->form; ?>",
						"multiple" => <?php if(isset($field->multiple) && $field->multiple) echo "true"; else echo "false"; ?>,
						"options" => array(
<?php 						foreach($field->options as $option): ?>
							"<?php echo $option->value; ?>" => "<?php echo $option->name; ?>",
<?php 						endforeach; ?>
						),
						"toggle" => array(
<?php 						foreach($field->options as $option): ?>
							"<?php echo $option->value; ?>" => array(
								"fields" => array(<?php $count = 0; foreach($option->toggle->fields as $f){ if($count !=0) echo ", "; echo "'".$f."'"; $count++; }  ?>),
								"sections" => array(<?php $count = 0; foreach($option->toggle->sections as $f){ if($count !=0) echo ", "; echo "'".$f."'"; $count++; }  ?>),
								"tabs" => array(<?php $count = 0; foreach($option->toggle->tabs as $f){ if($count !=0) echo ", "; echo "'".$f."'"; $count++;  }  ?>),
							),
<?php 						endforeach; ?>
						)
					),
<?php 				endforeach; ?>
				),
			),
<?php 		endforeach; ?>
		),
	),	
<?php endforeach; ?>
);
<?php if(isset($_REQUEST['module'])): ?>

FLBuilder::register_module('<?php echo $module->class_name; ?>', $options);

/*
Module Import Code:

<?php 
	echo json_encode(array("code"=>$code,"module"=>$module));
?>

*/

<?php else: ?>

FLBuilder::register_settings_form('<?php echo $form_name; ?>', array( "title" => "<?php echo $modal_name; ?>", "tabs" => $options ));
/*
Form Settings Import Code:

<?php 
	echo json_encode(array("code"=>$code,"form_name"=>$form_name,"modal_name"=>$modal_name));
?>

*/

<?php endif; ?>