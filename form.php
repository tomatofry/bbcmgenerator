<!doctype html>
<html ng-app="bbcm" >
  <head>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.16/angular.min.js"></script>
    <script src="script.js"></script>
    <link rel="stylesheet" href="script.css">
  </head>
  <body ng-controller="MainController as g"  >



    <div style='float:right;margin-right:10px;'>
      <input type='text' ng-model='import_code' style='float:right;' placeholder='Paste import code here' />

    </div>


    <h2>Beaver Builder Custom Module Generator</h2>

      <ul>
        <li ng-repeat='(tid,tab) in code' >
            
            <span ng-click='code.splice($index,1)' class='close' >&#10005;</span>
            <input value='{{tab}}' class='code' readonly title='copy' />
            <input class='code' id='tab{{tid}}' title='paste'  ng-keyup='updateArray("tab"+tid,tab);' />

            <!-- 10006 10005 -->
            Tab Name: <input ng-model='tab.title'   />
            Slug: <input ng-model='tab.slug'  ng-focus='updateSlug(tab,"slug","title")' />

            <ul>
              <li ng-repeat='(sid,section) in tab.sections' >
                  <span ng-click='tab.sections.splice($index,1)' class='close' >&#10005;</span>
                  <input value='{{section}}' class='code' readonly title='copy' />
                  <input class='code' id='tab{{tid}}section{{sid}}' title='paste'  ng-keyup='updateArray("tab"+tid+"section"+sid,section);' />


                Section Name: <input ng-model='section.title'  />
                Slug: <input ng-model='section.slug'  ng-focus='updateSlug(section,"slug","title")' />
                <ul>
                  <div><b>Fields</b></div>

                  <li ng-repeat='field in section.fields' class='horizontal' >

                       <div> 
                        <span ng-click='section.fields.splice($index,1)' class='close' >&#10005;</span></div>
                        <input value='{{field}}' class='code' readonly title='copy' /></div>
                        <input class='code' id='tab{{tid}}section{{sid}}field{{$index}}' title='paste' ng-keyup='updateArray("tab"+tid+"section"+sid+"field"+$index,field);'  />
                      </div>


                      <table>
                        <tr>
                          <td>Label: </td>
                          <td><input ng-model='field.label' /></td>
                        </tr>

                        <tr>
                          <td>Slug: </td>
                          <td><input ng-model='field.slug'  ng-focus='updateSlug(field,"slug","label")' /></td>
                        </tr>
                        
                        <tr>
                        <tr>
                          <td>Type: </td>
                          <td>
                            <select ng-model='field.type' >
                              <option value='code' >Code</option>
                              <option value='color' >Color</option>
                              <option value='editor' >Editor</option>
                              <option value='icon' >Icon</option>
                              <option value='link' >Link</option>
                              <option value='multiple-photos' >Multiple Photos</option>
                              <option value='photo' >Photo</option>
                              <option value='post-type' >Post Type</option>
                              <option value='select' >Select</option>
                              <option value='text' >Text</option>
                              <option value='textarea' >Textarea</option>
                              <option value='video' >Video</option>
                              <option value='form' >Form</option>
                            </select>
                          </td>
                        </tr>
                       <tr ng-if='(field.type == "form")' >
                          <td >Form: </td>
                          <td>
                            <input ng-model='field.form' />
                            <a href='form.php' target='_blank' >Create</a>
                          </td>
                        </tr>

                        <tr ng-if='!(field.type == "icon" || field.type == "multiple-photos" || field.type == "photo" || field.type == "post-type"  || field.type == "video")' >
                          <td >Default: </td>
                          <td><input ng-model='field.default' /></td>
                        </tr>
                        <tr  ng-if='!(field.type == "icon" || field.type == "multiple-photos" || field.type == "photo" || field.type == "post-type"  || field.type == "video")'   >
                          <td>Maxlength: </td>
                          <td><input ng-model='field.maxlength' /></td>
                        </tr>
                        <tr ng-if='!(field.type == "icon" || field.type == "multiple-photos" || field.type == "photo" || field.type == "post-type"  || field.type == "video")'  >
                          <td>Rows: </td>
                          <td><input ng-model='field.rows' /></td>
                        </tr>
                        <tr ng-if='!(field.type == "icon" || field.type == "multiple-photos" || field.type == "photo" || field.type == "post-type"  || field.type == "video")'  >
                          <td>Size: </td>
                          <td><input ng-model='field.size' /></td>
                        </tr>
                        <tr ng-if='!(field.type == "icon" || field.type == "multiple-photos" || field.type == "photo" || field.type == "post-type"  || field.type == "video")'  >
                          <td>Placeholder: </td>
                          <td><input ng-model='field.placeholder' /></td>
                        </tr>
                        <tr>
                          <td>Class: </td>
                          <td><input ng-model='field.class' /></td>
                        </tr>
                        <tr>
                          <td>Description: </td>
                          <td><input ng-model='field.description' /></td>
                        </tr>
                        <tr>
                          <td>Help: </td>
                          <td><input ng-model='field.help' /></td>
                        </tr>
                        <tr>
                          <td>Multiple: </td>
                          <td><input type='checkbox' ng-model='field.multiple' /></td>
                        </tr>
                        <tr ng-if='(field.type == "icon")'  >
                          <td>Show Remove: </td>
                          <td><input type='checkbox' ng-model='field.show_remove' /></td>
                        </tr>
                        <tr ng-if='(field.type == "editor")'  >
                          <td>Media Button: </td>
                          <td><input type='checkbox' ng-model='field.media_buttons' /></td>
                        </tr>
                        <tr  ng-if='(field.type == "color")'  >
                          <td>Show Reset: </td>
                          <td><input type='checkbox' ng-model='field.show_reset' /></td>
                        </tr>
                        <tr ng-if='(field.type == "code")'  >
                          <td>Editor: </td>
                          <td>
                            <select ng-model='field.show_reset' >
                              <option value='html' >HTML</option>
                              <option value='text' >Text</option>
                            </select>
                          </td>
                        </tr>
                        <tr ng-if='(field.type == "select")'  >
                          <td>Options: </td>
                          <td>
                            <ul ng-if='field.options.length' >
                              <li ng-repeat='option in field.options' >
                                 <span ng-click='field.options.splice($index,1)' class='close' >&#10005;</span>
                                <table><tr><td>Value</td><td> <input ng-model='option.value' /></td></tr>
                                <tr><td>Name:</td><td> <input ng-model='option.name' /></td></tr></table>
                              </li>
                            </ul>
                            <button type='button' ng-click='field.options.push({"value":"","name":"","toggle":{"fields":[],"sections":[],"tabs":[]}});' >Add Option</button>
                          </td>
                        </tr>
                        <tr ng-if='(field.type == "select")'  >
                          <td>Toggle: </td>
                          <td>
                            

                            <label><input type='checkbox' ng-model='field.options.toggle_enabled' /> Enable Toggles</label>
                            <ul ng-if='field.options.length && field.options.toggle_enabled' >                              
                              <li ng-repeat='option in field.options' >
                                

                                <table>
                                  <tr><td>Option:</td><td>{{option.name}}</td></tr>
                                  <tr><td>Fields:</td><td>
                                    <div ng-repeat='field_toggle in option.toggle.fields track by $index' >
                                      <span ng-click='option.toggle.fields.splice($index,1)' class='close' >&#10005;</span>
                                      <select  ng-model='option.toggle.fields[$index]' >
                                          <option ng-repeat='f in getFieldsList()' value='{{f.slug}}' ng-if='f.slug != field.slug' >{{f.label}}</option>
                                      </select>
                                    </div>
                                    <div><button type='button' style='margin-top:0px;' ng-click='option.toggle.fields.push("");' >Add Field</button></div>
                                  </td></tr>
                                  <tr><td>Sections:</td><td>
                                    
                                    <div ng-repeat='section_toggle in option.toggle.sections track by $index'  >
                                      <span ng-click='option.toggle.sections.splice($index,1)' class='close' >&#10005;</span>
                                      <select  ng-model='option.toggle.sections[$index]' >
                                        <option ng-repeat='f in getSectionsList()' value='{{f.slug}}' ng-if='f.slug != section.slug'  >{{f.title}}</option>
                                      </select>
                                    </div>
                                    <div><button type='button' style='margin-top:0px;' ng-click='option.toggle.sections.push("");' >Add Section</button></div>
                                  </td></tr>
                                  <tr><td>Tabs:</td><td>
                                    <div ng-repeat='tab_toggle in option.toggle.tabs track by $index' >
                                      <span ng-click='option.toggle.tabs.splice($index,1)' class='close' >&#10005;</span>
                                      <select  ng-model='option.toggle.tabs[$index]' >
                                        <option ng-repeat='f in getTabsList()' value='{{f.slug}}' ng-if='f.slug != tab.slug'  >{{f.title}}</option>
                                      </select>
                                    </div>
                                    <div>
                                      <button type='button' style='margin-top:0px;' ng-click='option.toggle.tabs.push("");' >Add Tabs</button>
                                    </div>
                                  </td></tr>

                                </table>
                              </li>
                            </ul>
                          </td>
                        </tr>
                        <tr ng-if='!(field.type == "icon" || field.type == "multiple-photos" || field.type == "photo" || field.type == "post-type"  || field.type == "video")' >
                          <td >Preview: </td>
                          <td>
                            <label><input type='checkbox' ng-model='field.preview_enabled' /> Enable Preview</label>
                            <table ng-if='field.preview_enabled' >
                              <tr><td>
                                Type:
                              </td><td>
                                <select ng-model='field.preview.type' >
                                  <option value='text' >Text</option>
                                  <option value='css' >CSS</option>
                                </select>
                              </td></tr>
                              <tr><td>
                                Selector Class:
                              </td><td>
                                <input type='text' ng-model='field.preview.selector' />
                              </td></tr>
                              <tr ng-if='field.preview.type == "css"' ><td>
                                Property:
                              </td><td>
                                <input type='text' ng-model='field.preview.property' />
                              </td></tr>
                              <tr ng-if='field.preview.type == "css"' ><td>
                                Unit:
                              </td><td>
                                <input type='text' ng-model='field.preview.unit' />
                              </td></tr>
                            </table>
                            
                           
                          </td>
                        </tr>
                      </table>

     

                  </li>
                  <div>
                    <button type='button' ng-click='addField(section)' >Add Field</button>
                  </div>
                </ul>

              </li>
              <button type='button' ng-click='tab.sections.push({"slug":"","title":"","fields":[]});' >Add Section</button>
            </ul>
            
        </li>
        <button type='button' ng-click='code.push({"slug":"","title":"","sections":[]});' >Add Tab</button>
      </ul>
      <hr />
      <b>Module Settings</b>
      <table>        
        <tr><td>Name:</td><td><input ng-model='module.name' /></td></tr>
        <tr><td>Class Name:</td><td><input ng-model='module.class_name' /></td></tr>
        <tr><td>Description:</td><td><input ng-model='module.description' /></td></tr>
        <tr><td>Category:</td><td><input ng-model='module.category' /></td></tr>
        <tr><td>Editor Export:</td><td><input type='checkbox' ng-model='module.editor_export' /></td></tr>
        <tr><td>Enabled:</td><td><input type='checkbox'  ng-model='module.enabled' /></td></tr>
        <tr>
          <td colspan='2' >
              <form method='post' target="_blank" action='generate.php' style='float:right;' >
              <input type='hidden' name='code' value='{{code}}' />
              <input type='hidden' name='module' value='{{module}}' />
              <button style='margin:0;' type='submit' >Generate</button>
            </form>
          </td>
        </tr>

      </table>


  </body>
</html>